//
//  NYCSchoolsDataSource.swift
//  20200914-SadiyaJabeen-NYCSchools
//
//  Created by Sadiya Syeda on 9/11/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import UIKit
import MapKit

final class NYCSchoolsDataSource: NSObject {
    private weak var parentVC: NYCSchoolsViewController?
    
    init(_ parentVC: NYCSchoolsViewController) {
        self.parentVC = parentVC
    }
}

extension NYCSchoolsDataSource: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return parentVC?.nycViewModel?.getNumberOfSections() ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return parentVC?.nycViewModel?.getNumberOfRows() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NYCSchoolsTableViewCell", for: indexPath) as? NYCSchoolsTableViewCell, let cellModel = parentVC?.nycViewModel?.modelFor(indexPath.row) else { return UITableViewCell() }
        
        cell.configure(cellModel, delegate: self)
        
        return cell
    }
}

// MARK: NYCSchoolsTableViewCellDelegate

extension NYCSchoolsDataSource: NYCSchoolsTableViewCellDelegate {
    func phoneButtonTapped(_ phoneNumber: String) {
        if let url = URL(string: "tel://\(String(describing: phoneNumber))"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else{
            let alertView = UIAlertController(title: "Oops!", message: "Cannot make calls on simulator. Please use a real device to call \(phoneNumber)", preferredStyle: .alert)
            let okayAction = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
            alertView.addAction(okayAction)
            parentVC?.present(alertView, animated: true, completion: nil)
        }
    }
    
    func navigateToAddressButtonTapped(_ schoolName: String, latitude: Double, longitude: Double) {
        let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate))
        mapItem.name = schoolName
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
    }
}
