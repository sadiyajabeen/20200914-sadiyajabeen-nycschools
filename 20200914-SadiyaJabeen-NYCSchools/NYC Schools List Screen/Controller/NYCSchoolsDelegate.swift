//
//  NYCSchoolsDelegate.swift
//  20200914-SadiyaJabeen-NYCSchools
//
//  Created by Sadiya Syeda on 9/11/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import UIKit

final class NYCSchoolsDelegate: NSObject {
    private weak var parentVC: NYCSchoolsViewController?
    
    init(_ parentVC: NYCSchoolsViewController) {
        self.parentVC = parentVC
    }
    
    func loadMoreResults() {
        guard let parentVC = parentVC else { return }
        if parentVC.currentPage <= parentVC.maxPages {
            parentVC.currentPage = parentVC.currentPage + 1
            parentVC.fetchNYCSchools()
        }
    }
}

extension NYCSchoolsDelegate: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cellModel = parentVC?.nycViewModel?.modelFor(indexPath.row) else { return }
        let dbn = cellModel.dbn
        
        guard !dbn.isEmpty else {
            print("Empty dbn returned")
            
            return
        }
        
        let detailModel = NYCSchoolDetailModel(dbn: dbn,
                                               schoolName: cellModel.schoolName,
                                               address: cellModel.address,
                                               overviewParagraph: cellModel.overviewParagraph,
                                               latitude: cellModel.latitude,
                                               longitude: cellModel.longitude,
                                               phone: cellModel.phone,
                                               website: cellModel.website)
        
        let detailsVC = NYCSchoolDetailsViewController(detailModel)
        parentVC?.navigationController?.pushViewController(detailsVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let parentVC = parentVC else { return }
        
        if indexPath.row == parentVC.nycSchools.count - 1 {
            loadMoreResults() // Pagination call to fetch only 5 items at a time.
        }
    }
}
