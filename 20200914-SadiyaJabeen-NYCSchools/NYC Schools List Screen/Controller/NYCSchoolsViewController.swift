//
//  ViewController.swift
//  20200914-SadiyaJabeen-NYCSchools
//
//  Created by Sadiya Syeda on 9/11/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import UIKit

final class NYCSchoolsViewController: UIViewController {
    // MARK: TableView
    @IBOutlet private weak var tableView: UITableView!
    private var dataSource: NYCSchoolsDataSource?
    private var delegate: NYCSchoolsDelegate?
    
    // Search and Refresh Control
    private let searchController = UISearchController(searchResultsController: nil)
    @IBOutlet private weak var refreshControl: UIBarButtonItem!
    private let activityView = UIActivityIndicatorView(style: .whiteLarge)
    
    // MARK:- Schools list
    private var filteredSchools: [NYCSchool] = []
    
    var nycSchools: [NYCSchool] = []

    var nycViewModel: NYCSchoolsViewModel? {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
    
    // MARK:- Pagination
    var currentPage = 0
    var maxPages = 50
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchController()
        setUpTableView()
        fetchNYCSchools()
    }
}

// MARK:- Private helper functions
private extension NYCSchoolsViewController {
    func setUpTableView() {
        tableView.separatorStyle = .none
        dataSource = NYCSchoolsDataSource(self)
        delegate = NYCSchoolsDelegate(self)
        tableView.dataSource = dataSource
        tableView.delegate = delegate
        tableView.register(UINib(nibName: "NYCSchoolsTableViewCell", bundle: nil),
                           forCellReuseIdentifier: "NYCSchoolsTableViewCell")
    }
    
    func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Schools"
        searchController.searchBar.tintColor = .white
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    @IBAction func refreshSchoolsTapped(_ sender: Any) {
        fetchNYCSchools()
    }
    
    func showActivityIndicator() {
        activityView.center = self.view.center
        activityView.color = .red
        self.view.addSubview(activityView)
        activityView.startAnimating()
    }
    
    func hideActivityIndicator(){
        DispatchQueue.main.async { [weak self] in
            self?.activityView.stopAnimating()
        }
    }
}

// MARK: Api and Pagination call
extension NYCSchoolsViewController {
    func fetchNYCSchools() {
        let nycSchoolsEndpoint = NYCSchoolsEndpoint(currentPage: currentPage)
        let nycService = NYCSchoolsService(nycSchoolsEndpoint)
        
        showActivityIndicator()
        
        nycService.fetchSchools { [weak self] (result) in
            self?.hideActivityIndicator()
            switch result {
            case let .success(nycSchools):
                guard let self = self else { return }
                self.filteredSchools = nycSchools
                
                if self.currentPage > 0 {
                   self.nycSchools = self.nycSchools + nycSchools
                } else {
                    self.nycSchools = nycSchools
                }
                
                self.nycViewModel = NYCSchoolsViewModel(self.nycSchools, filteredSchools: self.filteredSchools)
            case let .fail(error):
                print(error.description)
            }
        }
    }
}

// MARK: Handle Search Controller filtering
extension NYCSchoolsViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text else { return }
        let nycSchoolsFilterResultsHelper = NYCSchoolsFilterResultsHelper(nycSchools)
        
        nycSchoolsFilterResultsHelper.filterResults(searchText) { [weak self] (filteredSchools) in
            self?.filteredSchools = filteredSchools
            self?.nycViewModel = NYCSchoolsViewModel(nycSchools, filteredSchools: filteredSchools, showAllResults: false)
        }
    }
}
