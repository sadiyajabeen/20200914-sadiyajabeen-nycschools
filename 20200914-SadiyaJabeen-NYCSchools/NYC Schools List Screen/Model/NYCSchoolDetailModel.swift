//
//  NYCSchoolDetailModel.swift
//  20200914-SadiyaJabeen-NYCSchools
//
//  Created by Sadiya Syeda on 9/11/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import Foundation

struct NYCSchoolDetailModel {
    var dbn: String
    var schoolName: String?
    var address: String?
    var overviewParagraph: String?
    var latitude: Double?
    var longitude: Double?
    var phone: String?
    var website: String
}
