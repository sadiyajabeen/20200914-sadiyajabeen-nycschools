//
//  NYCSchools.swift
//  20200914-SadiyaJabeen-NYCSchools
//
//  Created by Sadiya Syeda on 9/11/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import Foundation

struct NYCSchool: Codable {
    var dbn: String?
    var schoolName: String?
    var overviewParagraph: String?
    var primaryAddressLine1: String?
    var city: String?
    var zip: String?
    var stateCode: String?
    var website: String?
    var phoneNumber: String?
    var latitude: String?
    var longitude: String?
    var grades: String?
    var totalStudents: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case primaryAddressLine1 = "primary_address_line_1"
        case city, zip
        case stateCode = "state_code"
        case website
        case phoneNumber = "phone_number"
        case latitude
        case longitude
        case grades = "finalgrades"
        case totalStudents = "total_students"
    }
}
