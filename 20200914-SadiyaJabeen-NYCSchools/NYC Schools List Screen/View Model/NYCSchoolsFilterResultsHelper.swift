//
//  NYCSchoolsFilterResultsHelper.swift
//  20200914-SadiyaJabeen-NYCSchools
//
//  Created by Sadiya Syeda on 9/11/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import Foundation

final class NYCSchoolsFilterResultsHelper {
    private let nycSchools: [NYCSchool]
    init(_ nycSchools: [NYCSchool]) {
        self.nycSchools = nycSchools
    }
    
    func filterResults(_ searchText: String, completion: (([NYCSchool]) -> Void)) {
        guard !searchText.isEmpty else {
            completion(nycSchools)
            
            return
        }
        
        let filteredResults = nycSchools.filter { ($0.schoolName ?? "").lowercased().contains(searchText.lowercased()) || ($0.city ?? "").lowercased().contains(searchText.lowercased())} 
        
        completion(filteredResults)
    }
}
