//
//  NYCSchoolsViewModel.swift
//  20200914-SadiyaJabeen-NYCSchools
//
//  Created by Sadiya Syeda on 9/11/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import Foundation

struct NYCSchoolsCellModel {
    var dbn: String
    var schoolName: String
    var address: String
    var grades: String?
    var phone: String
    var latitude: Double
    var longitude: Double
    var overviewParagraph: String
    var website: String
    var totalStudents: String
}

struct NYCSchoolsViewModel {
    private let nycSchools: [NYCSchool]
    private let filteredSchools: [NYCSchool]
    private let showAllResults: Bool
    
    init(_ nycSchools: [NYCSchool], filteredSchools: [NYCSchool], showAllResults: Bool = true) {
        self.nycSchools = nycSchools
        self.filteredSchools = filteredSchools
        self.showAllResults = showAllResults
    }
    
    func getNumberOfSections() -> Int {
        return 1
    }
    
    func getNumberOfRows() -> Int {
        return showAllResults ? nycSchools.count : filteredSchools.count
    }
    
    func modelFor(_ row: Int) -> NYCSchoolsCellModel {
        let schoolsList = showAllResults ? nycSchools : filteredSchools
        
        let school = schoolsList[row]
        let dbn = school.dbn ?? ""
        let schoolName = school.schoolName ?? "Not Available"
        
        let schoolPrimaryAddressLine1: String
        
        if let primaryAddressLine1 = school.primaryAddressLine1 {
            schoolPrimaryAddressLine1 = "\(primaryAddressLine1), "
        } else {
            schoolPrimaryAddressLine1 = ""
        }
        
        let cityText: String
        
        if let city = school.city {
            cityText = "\(city), "
        } else {
            cityText = ""
        }
        
        let zipText: String
        
        if let zip = school.zip {
            zipText = "\(zip), "
        } else {
            zipText = ""
        }
        
        let stateCode = school.stateCode ?? ""
        
        var schoolFullAddress = "\(schoolPrimaryAddressLine1)\(cityText)\(zipText)\(stateCode)"
        
        schoolFullAddress = schoolFullAddress.isEmpty ? "Address Not available" : schoolFullAddress
        
        let gradesText = "Grades: \(school.grades ?? "N/A")"
        let phone = school.phoneNumber ?? "N/A"
        let phoneText = "📞 \(phone)"
        let latitudeDouble: Double
        
        if let latitude = school.latitude, let latitudeNumber = Double(latitude) {
            latitudeDouble = latitudeNumber
        } else {
            latitudeDouble = 0
        }
        
        let longitudeDouble: Double
            
        if let longitude = school.longitude, let longitudeNumber = Double(longitude) {
            longitudeDouble = longitudeNumber
        } else {
            longitudeDouble = 0
        }
        
        let overviewPara = school.overviewParagraph ?? "No Overview Available"
        let website = school.website ?? ""
        
        let totalStudentsText: String
        
        if let totalStudents = school.totalStudents {
            totalStudentsText = "\(totalStudents) students"
        } else {
            totalStudentsText = ""
        }
        
        
        return NYCSchoolsCellModel(dbn: dbn,
                                   schoolName: schoolName,
                                   address: schoolFullAddress,
                                   grades: gradesText,
                                   phone: phoneText,
                                   latitude: latitudeDouble,
                                   longitude: longitudeDouble,
                                   overviewParagraph: overviewPara,
                                   website: website,
                                   totalStudents: totalStudentsText)
        
    }
}
