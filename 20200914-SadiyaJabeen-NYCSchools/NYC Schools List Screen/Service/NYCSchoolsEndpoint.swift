//
//  NYCSchoolsEndpoint.swift
//  20200914-SadiyaJabeen-NYCSchools
//
//  Created by Sadiya Syeda on 9/11/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import Foundation

enum HttpMethod: String {
    case post = "POST"
    case put = "PUT"
    case get = "GET"
    case delete = "DELETE"
}

struct NYCSchoolsUrl {
    static let schoolList: URL? = URL(string: "https://data.cityofnewyork.us/resource/97mf-9njv.json")
    static let schoolDetails: URL? = URL(string: "https://data.cityofnewyork.us/resource/734v-jeq5.json")
}

final class NYCSchoolsEndpoint {
    private let url: URL?
    private let paginationLimit: Int = 10
    
    var request: URLRequest? {
        guard let url = url else { return nil }
        var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
        urlComponents?.queryItems = queryItems
        
        guard let fullUrl = urlComponents?.url else {
            return nil
        }
        
        var request = URLRequest(url: fullUrl)
        request.addValue("eGzBY2pJEQDB75MxXtD94q2Pf", forHTTPHeaderField: "X-App-Token")
        
        return request
    }
    
    var httpMethod: HttpMethod? = HttpMethod.get
    
    var queryItems: [URLQueryItem] = []
    
    var messageBodyDictionary: [String : Any]?
    
    init(_ url: URL? = NYCSchoolsUrl.schoolList, currentPage: Int) {
        self.url = url
        self.queryItems.append(URLQueryItem(name: "$offset", value: "\(currentPage * paginationLimit)"))
        self.queryItems.append(URLQueryItem(name: "$limit", value: "\(paginationLimit)"))
    }
}
