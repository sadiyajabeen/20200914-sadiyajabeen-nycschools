//
//  NYCSchoolsService.swift
//  20200914-SadiyaJabeen-NYCSchools
//
//  Created by Sadiya Syeda on 9/11/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import Foundation

final class NYCSchoolsService {
    enum NYCSchoolsResultType {
        case success(nycSchools: [NYCSchool])
        case fail(error: String)
    }
    
    private let nycSchoolsEndpoint: NYCSchoolsEndpoint
    
    init(_ nycSchoolsEndpoint: NYCSchoolsEndpoint) {
        self.nycSchoolsEndpoint = nycSchoolsEndpoint
    }
    
    func fetchSchools(_ completion: @escaping (NYCSchoolsResultType) -> Void) {
        guard let request = nycSchoolsEndpoint.request else {
            completion(.fail(error: "Invalid Endpoint"))
            return
        }
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                completion(.fail(error: error.localizedDescription))
                
                return
            }
            
            guard let data = data else {
                completion(.fail(error: "Invalid data object returned"))
                
                return
            }
            
            guard let nycSchools = try? JSONDecoder().decode([NYCSchool].self, from: data) else {
                completion(.fail(error: "Failed to parse data object to array of NYCSchools"))
                
                return
            }
            
            completion(.success(nycSchools: nycSchools))
        }.resume()
    }
}
