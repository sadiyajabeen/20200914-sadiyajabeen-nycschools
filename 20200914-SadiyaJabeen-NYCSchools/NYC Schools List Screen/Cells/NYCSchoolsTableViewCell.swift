//
//  NYCSchoolsTableViewCell.swift
//  20200914-SadiyaJabeen-NYCSchools
//
//  Created by Sadiya Syeda on 9/11/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import UIKit

protocol NYCSchoolsTableViewCellDelegate: class {
    func phoneButtonTapped(_ phoneNumber: String)
    func navigateToAddressButtonTapped(_ schoolName: String, latitude: Double, longitude: Double)
}
    
final class NYCSchoolsTableViewCell: UITableViewCell {
    @IBOutlet private weak var cardView: UIView!
    @IBOutlet private weak var schoolNameLabel: UILabel!
    @IBOutlet private weak var addressLabel: UILabel!
    @IBOutlet weak var gradesLabel: UILabel!
    @IBOutlet private weak var phoneButton: UIButton!
    @IBOutlet private weak var navigateToAddress: UIButton!
    @IBOutlet private weak var totalStudentsLabel: UILabel!
    private var currentCellModel: NYCSchoolsCellModel?
    private var delegate: NYCSchoolsTableViewCellDelegate?
    
    private struct Styling {
        static let buttonCornerRadius: CGFloat = 10
        
        struct CardView {
            static let cornerRadius: CGFloat = 15
            static let shadowColor = UIColor.black.cgColor
            static let shadowOffset = CGSize(width: 0, height: 2)
            static let opacity: Float = 0.8
            static let shadowRadius: CGFloat = 3
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(_ cellModel: NYCSchoolsCellModel, delegate: NYCSchoolsTableViewCellDelegate) {
        currentCellModel = cellModel
        self.delegate = delegate
        cardView.backgroundColor = .white
        schoolNameLabel.text = cellModel.schoolName
        addressLabel.text = cellModel.address
        gradesLabel.text = cellModel.grades
        totalStudentsLabel.text = cellModel.totalStudents
        phoneButton.setTitle(cellModel.phone, for: .normal)
    }
    
    @IBAction private func phoneButtonTapped(_ sender: Any) {
        guard let phone = currentCellModel?.phone else { return }
        delegate?.phoneButtonTapped(phone)
    }
    
    @IBAction private func navigateToAddressButtonTapped(_ sender: Any) {
        guard let schoolName = currentCellModel?.schoolName,
            let lat = currentCellModel?.latitude,
            let long = currentCellModel?.longitude else { return }
        delegate?.navigateToAddressButtonTapped(schoolName, latitude: lat, longitude: long)
    }
}

private extension NYCSchoolsTableViewCell {
    func setupUI() {
        selectionStyle = .none
        setupDropShadow()
        setupRoundedCornerButtons()
    }
    
    func setupDropShadow() {
        cardView?.layer.cornerRadius = Styling.CardView.cornerRadius
        cardView?.layer.shadowColor = Styling.CardView.shadowColor
        cardView?.layer.shadowOffset = Styling.CardView.shadowOffset
        cardView?.layer.shadowOpacity = Styling.CardView.opacity
        cardView?.layer.shadowRadius = Styling.CardView.shadowRadius
        cardView?.layer.masksToBounds = false
    }
    
    func setupRoundedCornerButtons() {
        phoneButton.layer.cornerRadius = Styling.buttonCornerRadius
        phoneButton.clipsToBounds = true
        navigateToAddress.layer.cornerRadius = Styling.buttonCornerRadius
        navigateToAddress.clipsToBounds = true
    }
}
