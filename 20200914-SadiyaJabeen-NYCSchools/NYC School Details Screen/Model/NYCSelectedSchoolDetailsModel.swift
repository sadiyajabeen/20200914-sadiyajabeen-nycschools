//
//  NYCSelectedSchoolDetailsModel.swift
//  20200914-SadiyaJabeen-NYCSchools
//
//  Created by Sadiya Syeda on 9/11/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import Foundation

struct NYCSelectedSchoolDetailsModel: Codable {
    var dbn: String?
    var schoolName: String?
    var numOfSatTestTakers: String?
    var satCriticalReadingAvgScore: String?
    var satMathAvgScore: String?
    var satWritingAvgScore: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
}
