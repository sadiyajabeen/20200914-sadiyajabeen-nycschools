//
//  NYCDetailsViewModel.swift
//  20200914-SadiyaJabeen-NYCSchools
//
//  Created by Sadiya Syeda on 9/12/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import Foundation

enum NYCSchoolDetailsRow: Int, CaseIterable {
    case satResults
    case overview
    case contact
    case schoolMap
}

enum NYCSchoolDetailsSection: Int, CaseIterable {
    case satResults
    case overview
    case contact
    case schoolMap
}

struct NYCDetailsScreenModel {
    let schoolName: String?
    let address: String?
    let satCriticalReadingAvgScore: String?
    let satMathAvgScore: String?
    let satWritingAvgScore: String?
    let overviewParagraph: String?
    let phone: String?
    let website: String?
    let latitude: Double?
    let longitude: Double?
}

struct NYCDetailsViewModel {
    
    private let selectedDetailsModel: NYCSelectedSchoolDetailsModel
    private let nycSchoolDetailModel: NYCSchoolDetailModel
    
    init(_ selectedDetailsModel: NYCSelectedSchoolDetailsModel, nycSchoolDetailModel: NYCSchoolDetailModel) {
        self.selectedDetailsModel = selectedDetailsModel
        self.nycSchoolDetailModel = nycSchoolDetailModel
    }
    
    func getNumberOfSections() -> Int {
        return NYCSchoolDetailsSection.allCases.count
    }
    
    func getNumberOfRows(_ section: NYCSchoolDetailsSection) -> Int {
        switch section {
        case .satResults, .overview, .contact, .schoolMap:
            return 1
        }
    }
    
    func getScreenModel() -> NYCDetailsScreenModel {
        let schoolName = selectedDetailsModel.schoolName ?? "N/A"
        let address = nycSchoolDetailModel.address ?? "N/A"
        let addressText = "Address: \(address)"
        
        let satCriticalReadingAvgScore = selectedDetailsModel.satCriticalReadingAvgScore ?? "N/A"
        let satCriticalReadingAvgScoreText = "Critical Reading Avg Score = \(satCriticalReadingAvgScore)"
        
        let satMathAvgScore = selectedDetailsModel.satMathAvgScore ?? "N/A"
        let satMathAvgScoreText = "SAT Math Avg Score = \(satMathAvgScore)"
        
        let satWritingAvgScore = selectedDetailsModel.satWritingAvgScore ?? "N/A"
        let satWritingAvgScoreText = "SAT Writing Average Score = \(satWritingAvgScore)"
        
        let overviewParagraph = nycSchoolDetailModel.overviewParagraph ?? "This school doesn't have overview"
        let phone = nycSchoolDetailModel.phone ?? "N/A"
        let phoneText = "Phone: \(phone)"
        
        let website = nycSchoolDetailModel.website
        
        let latitudeDouble: Double
        
        if let latitude = nycSchoolDetailModel.latitude {
            latitudeDouble = Double(latitude)
        } else {
            latitudeDouble = 0
        }
        
        let longitudeDouble: Double
        
        if let longitude = nycSchoolDetailModel.longitude {
            longitudeDouble = Double(longitude)
        } else {
            longitudeDouble = 0
        }
        
        return NYCDetailsScreenModel(schoolName: schoolName,
                                     address: addressText,
                                     satCriticalReadingAvgScore: satCriticalReadingAvgScoreText,
                                     satMathAvgScore: satMathAvgScoreText,
                                     satWritingAvgScore: satWritingAvgScoreText,
                                     overviewParagraph: overviewParagraph,
                                     phone: phoneText,
                                     website: website,
                                     latitude: latitudeDouble,
                                     longitude: longitudeDouble)
    }
}
