//
//  NYCSchoolDetailsViewController.swift
//  20200914-SadiyaJabeen-NYCSchools
//
//  Created by Sadiya Syeda on 9/11/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

final class NYCSchoolDetailsViewController: UIViewController {
     lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .white
        tableView.sectionHeaderHeight = 40
        
        return tableView
    }()
    
    private var dataSource: NYCSchoolDetailDataSource?
    private var nycSchoolDetailModel: NYCSchoolDetailModel
    
    var nycDetailsViewModel: NYCDetailsViewModel? {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
    
    init(_ nycSchoolDetailModel: NYCSchoolDetailModel) {
        self.nycSchoolDetailModel = nycSchoolDetailModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        navigationItem.titleView = setupScreenTitle()
        fetchNYCSchoolDetails()
    }
    
    func setupScreenTitle() -> UILabel {
        let titleLabel = UILabel()
        titleLabel.text = nycSchoolDetailModel.schoolName ?? ""
        titleLabel.numberOfLines = 2
        titleLabel.font = UIFont.boldSystemFont(ofSize: 17.0)
        
        return titleLabel
    }
}

private extension NYCSchoolDetailsViewController {
    func setupTableView() {
        dataSource = NYCSchoolDetailDataSource(self)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        registerCells()
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    func registerCells() {
        tableView.register(UINib(nibName: "NYCSatResultsTableViewCell", bundle: nil),
                           forCellReuseIdentifier: "NYCSatResultsTableViewCell")
        tableView.register(UINib(nibName: "NYCSchoolOverviewTableViewCell", bundle: nil), forCellReuseIdentifier: "NYCSchoolOverviewTableViewCell")
        tableView.register(UINib(nibName: "NYCSchoolContactDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "NYCSchoolContactDetailsTableViewCell")
        tableView.register(UINib(nibName: "NYCSchoolMapTableViewCell", bundle: nil), forCellReuseIdentifier: "NYCSchoolMapTableViewCell")
        
    }
    
    func fetchNYCSchoolDetails() {
        let queryItem = URLQueryItem(name: "dbn", value: nycSchoolDetailModel.dbn)
        let nycSchoolDetailsEndpoint = NYCSchoolDetailsEndpoint(queryItems: [queryItem])
        let nycSchoolDetailService = NYCSchooDetailService(nycSchoolDetailsEndpoint)
        
        nycSchoolDetailService.fetchSchoolDetail { [weak self] (result) in
            guard let self = self else { return }
            
            switch result {
            case let .success(nycSelectedSchoolDetailsModel):
                self.nycDetailsViewModel = NYCDetailsViewModel(nycSelectedSchoolDetailsModel, nycSchoolDetailModel: self.nycSchoolDetailModel)
            case let .fail(error):
                print(error.description)
                let emptySATModel = NYCSelectedSchoolDetailsModel(dbn: nil, schoolName: nil, numOfSatTestTakers: nil, satCriticalReadingAvgScore: nil, satMathAvgScore: nil, satWritingAvgScore: nil)
                self.nycDetailsViewModel = NYCDetailsViewModel(emptySATModel, nycSchoolDetailModel: self.nycSchoolDetailModel)
            }
        }
    }
}
