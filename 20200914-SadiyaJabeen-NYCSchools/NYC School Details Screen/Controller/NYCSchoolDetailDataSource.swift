//
//  NYCSchoolDetailDataSource.swift
//  20200914-SadiyaJabeen-NYCSchools
//
//  Created by Sadiya Syeda on 9/12/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import UIKit
import SafariServices

final class NYCSchoolDetailDataSource: NSObject {
    private weak var parentVC: NYCSchoolDetailsViewController?
    
    init(_ parentVC: NYCSchoolDetailsViewController) {
        self.parentVC = parentVC
    }
}

enum NYCSchoolDetailsSectionHeaders: Int, CaseIterable {
    case satResults
    case overview
    case contact
    case schoolMap
}

extension NYCSchoolDetailDataSource: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return parentVC?.nycDetailsViewModel?.getNumberOfSections() ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let currentSection = NYCSchoolDetailsSection(rawValue: section) else {
            return 0
        }
        return parentVC?.nycDetailsViewModel?.getNumberOfRows(currentSection) ?? 0
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let currentSection = NYCSchoolDetailsSection(rawValue: section) else { return nil }
        
        let sectionHeaderView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.parentVC?.tableView.bounds.width ?? 0.0, height: 20.0))
        
        // Add label to the header view
        let sectionHeaderLabel = UILabel()
        sectionHeaderLabel.font = .boldSystemFont(ofSize: 20.0)
        sectionHeaderView.addSubview(sectionHeaderLabel)
        sectionHeaderView.backgroundColor = UIColor(displayP3Red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        sectionHeaderLabel.translatesAutoresizingMaskIntoConstraints = false
        sectionHeaderLabel.topAnchor.constraint(equalTo: sectionHeaderView.topAnchor, constant: 10).isActive = true
        sectionHeaderLabel.leadingAnchor.constraint(equalTo: sectionHeaderView.leadingAnchor, constant: 10).isActive = true
        sectionHeaderLabel.trailingAnchor.constraint(equalTo: sectionHeaderView.trailingAnchor, constant: -10).isActive = true
        
        switch currentSection {
        case .satResults:
            sectionHeaderLabel.text = "SAT scores"
        case .overview:
            sectionHeaderLabel.text = "Overview"
        case .contact:
            sectionHeaderLabel.text = "Contact"
        case .schoolMap:
            sectionHeaderLabel.text = "School Location"
        }

        return sectionHeaderView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let currentSection = NYCSchoolDetailsSection(rawValue: indexPath.section),
            let screenModel = parentVC?.nycDetailsViewModel?.getScreenModel() else { return UITableViewCell()}
        var cell = UITableViewCell()
        
        switch currentSection {
        case .satResults:
            guard let satCell = tableView.dequeueReusableCell(withIdentifier: "NYCSatResultsTableViewCell", for: indexPath) as? NYCSatResultsTableViewCell else { return cell }
            satCell.configure(screenModel)
            cell = satCell
        case .overview:
            guard let overviewCell = tableView.dequeueReusableCell(withIdentifier: "NYCSchoolOverviewTableViewCell", for: indexPath) as? NYCSchoolOverviewTableViewCell else { return cell }
            overviewCell.configure(screenModel)
            cell = overviewCell
        case .contact:
            guard let contactCell = tableView.dequeueReusableCell(withIdentifier: "NYCSchoolContactDetailsTableViewCell", for: indexPath) as? NYCSchoolContactDetailsTableViewCell else { return cell }
            contactCell.configure(screenModel, delegate: self)
            cell = contactCell
        case .schoolMap:
            guard let mapCell = tableView.dequeueReusableCell(withIdentifier: "NYCSchoolMapTableViewCell", for: indexPath) as? NYCSchoolMapTableViewCell else { return cell }
            mapCell.configure(screenModel)
            cell = mapCell
        }
        
        return cell
    }
}

extension NYCSchoolDetailDataSource: NYCSchoolContactDetailsTableViewCellDelegate {
    func showWebsite(_ url: String) {
        var stringUrl = url
        
        if !stringUrl.lowercased().hasPrefix("http://") {
            stringUrl = "https://\(stringUrl)"
        }
        
        guard let url = URL(string: stringUrl) else { return }
        
        let vc = SFSafariViewController(url: url)
        parentVC?.present(vc, animated: true, completion: nil)
    }
}
