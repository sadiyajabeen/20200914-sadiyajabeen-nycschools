//
//  NYCSchoolOverviewTableViewCell.swift
//  20200914-SadiyaJabeen-NYCSchools
//
//  Created by Sadiya Syeda on 9/12/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import UIKit

final class NYCSchoolOverviewTableViewCell: UITableViewCell {
    @IBOutlet private weak var nycSchoolSummaryLabel: UILabel!
    
    override func awakeFromNib() {
        selectionStyle = .none
    }
    
    func configure(_ nycSelectedModel: NYCDetailsScreenModel) {
        nycSchoolSummaryLabel.text = nycSelectedModel.overviewParagraph
    }
}
