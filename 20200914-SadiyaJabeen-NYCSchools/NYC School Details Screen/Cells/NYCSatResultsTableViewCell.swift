//
//  NYCSatResultsTableViewCell.swift
//  20200914-SadiyaJabeen-NYCSchools
//
//  Created by Sadiya Syeda on 9/11/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import UIKit

class NYCSatResultsTableViewCell: UITableViewCell {
    @IBOutlet private weak var schoolNameLabel: UILabel!
    @IBOutlet private weak var satReadingScoreLabel: UILabel!
    @IBOutlet private weak var satMathScoreLabel: UILabel!
    @IBOutlet private weak var satWritingScoreLabel: UILabel!
    
    override func awakeFromNib() {
        selectionStyle = .none
    }
    
    func configure(_ nycDetailsScreenModel: NYCDetailsScreenModel) {
        satReadingScoreLabel.text = nycDetailsScreenModel.satCriticalReadingAvgScore
        satMathScoreLabel.text = nycDetailsScreenModel.satMathAvgScore
        satWritingScoreLabel.text = nycDetailsScreenModel.satWritingAvgScore
    }
    
}
