//
//  NYCSchoolContactDetailsTableViewCell.swift
//  20200914-SadiyaJabeen-NYCSchools
//
//  Created by Sadiya Syeda on 9/12/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import UIKit

protocol NYCSchoolContactDetailsTableViewCellDelegate: class {
    func showWebsite(_ url: String)
}

final class NYCSchoolContactDetailsTableViewCell: UITableViewCell {
    @IBOutlet private weak var nycSchoolAddressLabel: UILabel!
    @IBOutlet private weak var nycSchoolPhoneLabel: UILabel!
    @IBOutlet private weak var nycSchoolWebsiteLabel: UILabel!
    private var nycDetailsScreenModel: NYCDetailsScreenModel?
    var delegate: NYCSchoolContactDetailsTableViewCellDelegate?
    
    override func awakeFromNib() {
        selectionStyle = .none
        
        let websiteTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.websiteTapped(_:)))
        nycSchoolWebsiteLabel.isUserInteractionEnabled = true
        nycSchoolWebsiteLabel.addGestureRecognizer(websiteTapGesture)
    }
    
    func configure(_ nycDetailsScreenModel: NYCDetailsScreenModel, delegate: NYCSchoolContactDetailsTableViewCellDelegate?) {
        self.nycDetailsScreenModel = nycDetailsScreenModel
        self.delegate = delegate
        nycSchoolAddressLabel.text = nycDetailsScreenModel.address
        nycSchoolPhoneLabel.text = nycDetailsScreenModel.phone
        nycSchoolWebsiteLabel.text = nycDetailsScreenModel.website
    }
    
    @objc func websiteTapped(_ sender: UIGestureRecognizer) {
        guard let websiteUrl = nycDetailsScreenModel?.website else { return }
        
        delegate?.showWebsite(websiteUrl)
    }
}
