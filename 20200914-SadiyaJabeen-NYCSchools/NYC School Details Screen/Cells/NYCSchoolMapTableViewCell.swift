//
//  NYCSchoolMapTableViewCell.swift
//  20200914-SadiyaJabeen-NYCSchools
//
//  Created by Sadiya Syeda on 9/12/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import UIKit
import MapKit

final class NYCSchoolMapTableViewCell: UITableViewCell {
    @IBOutlet private weak var nycSchoolMapView: MKMapView!
    
    override func awakeFromNib() {
        selectionStyle = .none
    }
    func configure(_ nycDetailsScreenModel: NYCDetailsScreenModel) {
        let schoolAnnotation = MKPointAnnotation()
        schoolAnnotation.coordinate = CLLocationCoordinate2D(latitude: nycDetailsScreenModel.latitude ?? 0,
                                                             longitude: nycDetailsScreenModel.longitude ?? 0)
        nycSchoolMapView.addAnnotation(schoolAnnotation)
        let span = MKCoordinateSpan(latitudeDelta: 0.001, longitudeDelta: 0.001)
        let region = MKCoordinateRegion(center: schoolAnnotation.coordinate, span: span)
        let adjustRegion = nycSchoolMapView.regionThatFits(region)
        nycSchoolMapView.setRegion(adjustRegion, animated:true)
    }
}
