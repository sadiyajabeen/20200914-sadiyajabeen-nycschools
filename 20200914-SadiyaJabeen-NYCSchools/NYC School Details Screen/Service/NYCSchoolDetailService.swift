//
//  NYCSchoolDetailService.swift
//  20200914-SadiyaJabeen-NYCSchools
//
//  Created by Sadiya Syeda on 9/12/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import Foundation

final class NYCSchooDetailService {
    enum NYCSchoolDetailResultType {
        case success(nycSchoolDetailModel: NYCSelectedSchoolDetailsModel)
        case fail(error: String)
    }
    
    private let nycSchoolDetailsEndpoint: NYCSchoolDetailsEndpoint
    
    init(_ nycSchoolDetailsEndpoint: NYCSchoolDetailsEndpoint) {
        self.nycSchoolDetailsEndpoint = nycSchoolDetailsEndpoint
    }
    
    func fetchSchoolDetail(_ completion: @escaping (NYCSchoolDetailResultType) -> Void) {
        guard let request = nycSchoolDetailsEndpoint.request else {
            completion(.fail(error: "Invalid Endpoint"))
            return
        }
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                completion(.fail(error: error.localizedDescription))
                
                return
            }
            
            guard let data = data else {
                completion(.fail(error: "Invalid data object returned"))
                
                return
            }
            
            guard let nycSchoolDetailsModels = try? JSONDecoder().decode([NYCSelectedSchoolDetailsModel].self, from: data), let selectedSchoolModel = nycSchoolDetailsModels.first else {
                completion(.fail(error: "Failed to parse data object to array of NYCSchools"))
                
                return
            }
            
            completion(.success(nycSchoolDetailModel: selectedSchoolModel))
            }.resume()
    }
}
