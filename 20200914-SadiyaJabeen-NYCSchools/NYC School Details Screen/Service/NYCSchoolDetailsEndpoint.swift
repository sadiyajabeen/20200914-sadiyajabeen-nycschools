//
//  NYCSchoolDetailsEndpoint.swift
//  20200914-SadiyaJabeen-NYCSchools
//
//  Created by Sadiya Syeda on 9/11/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import Foundation

final class NYCSchoolDetailsEndpoint {
    private var url: URL?
    
    var request: URLRequest? {
        guard let url = url else { return nil }
        var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
        urlComponents?.queryItems = queryItems
        
        guard let fullUrl = urlComponents?.url else {
            return nil
        }
        
        var request = URLRequest(url: fullUrl)
        request.addValue("eGzBY2pJEQDB75MxXtD94q2Pf", forHTTPHeaderField: "X-App-Token")
        
        return request
    }
    
    var httpMethod: HttpMethod? = HttpMethod.get
    
    let queryItems: [URLQueryItem]
    
    var messageBodyDictionary: [String : Any]?
    
    init(url: URL? = NYCSchoolsUrl.schoolDetails, queryItems: [URLQueryItem]) {
        self.url = url
        self.queryItems = queryItems
    }
}
