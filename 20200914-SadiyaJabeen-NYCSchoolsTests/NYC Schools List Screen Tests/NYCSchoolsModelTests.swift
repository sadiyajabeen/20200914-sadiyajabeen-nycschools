//
//  NYCSchoolsModelTests.swift
//  20200914-SadiyaJabeen-NYCSchoolsTests
//
//  Created by Sadiya Syeda on 9/12/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import XCTest
@testable import _0200914_SadiyaJabeen_NYCSchools

class NYCSchoolsModelTests: XCTestCase {
    private var validUrl: URL? {
        let bundle = Bundle(for: classForCoder)
        guard let url = bundle.url(forResource: "MockNycSchools", withExtension: "json") else {
            return nil
        }
        
        return url
    }
    
    private var urlWithInvalidFields: URL? {
        let bundle = Bundle(for: classForCoder)
        guard let url = bundle.url(forResource: "MockNycSchoolsInvalidMissingFields", withExtension: "json") else {
            return nil
        }
        
        return url
    }
    
    override func setUp() {}

    override func tearDown() {}

    func testNYCSchoolsModel() {
        let successfulEndpoint = NYCSchoolsEndpoint(validUrl, currentPage: 0)
        let successfulService = NYCSchoolsService(successfulEndpoint)
        
        successfulService.fetchSchools { (result) in
            switch result {
            case let .success(nycSchools):
                XCTAssertEqual(nycSchools.count, 3)
                
                // Tests for All valid fields
                let firstSchool = nycSchools[0]
                XCTAssertEqual(firstSchool.dbn, "02M260")
                XCTAssertEqual(firstSchool.schoolName, "Clinton School Writers & Artists, M.S. 260")
                XCTAssertEqual(firstSchool.primaryAddressLine1, "10 East 15th Street")
                XCTAssertEqual(firstSchool.city, "Manhattan")
                XCTAssertEqual(firstSchool.zip, "10003")
                XCTAssertEqual(firstSchool.stateCode, "NY")
                XCTAssertEqual(firstSchool.website, "www.theclintonschool.net")
                XCTAssertEqual(firstSchool.phoneNumber, "212-524-4360")
                XCTAssertEqual(firstSchool.latitude, "40.73653")
                XCTAssertEqual(firstSchool.longitude, "-73.9927")
            case let .fail(error):
                XCTAssertEqual(error.description, "Invalid Endpoint")
            }
        }
    }
    
    // Tests for Invalid type and Missing fields
    
    func testNYCSchoolsModelWithMissingAndInvalidFields() {
        let endpoint = NYCSchoolsEndpoint(urlWithInvalidFields, currentPage: 0)
        let service = NYCSchoolsService(endpoint)
        
        service.fetchSchools { (result) in
            switch result {
            case let .success(nycSchools):
                XCTAssertEqual(nycSchools.count, 1)
            case let .fail(error):
                XCTAssertEqual(error.description, "Failed to parse data object to array of NYCSchools")
            }
        }
    }
}
