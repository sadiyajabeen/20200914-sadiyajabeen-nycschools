//
//  NYCSchoolsViewModelCellModelTests.swift
//  20200914-SadiyaJabeen-NYCSchoolsTests
//
//  Created by Sadiya Syeda on 9/12/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import XCTest
@testable import _0200914_SadiyaJabeen_NYCSchools

class NYCSchoolsViewModelCellModelTests: XCTestCase {
    var viewModel: NYCSchoolsViewModel = {
        let schools = [NYCSchool(dbn: "012M1", schoolName: "Clifton High School", overviewParagraph: "Great School", primaryAddressLine1: "250 Vesey Street", city: "NY", zip: "10281", stateCode: "NY", website: "www.google.com", phoneNumber: "999-999-9999", latitude: "111.12", longitude: "120.11", grades: "6-12", totalStudents: "200"),
                       NYCSchool(dbn: "012M1", schoolName: nil, overviewParagraph: nil, primaryAddressLine1: nil, city: nil, zip: nil, stateCode: nil, website: nil, phoneNumber: nil, latitude: nil, longitude: nil, grades: nil, totalStudents: nil)]
        return NYCSchoolsViewModel(schools, filteredSchools: schools)
    }()
    
    override func setUp() {}

    override func tearDown() {}

    // Tests for ViewModel and Cell Model with Valid and Invalid fields
    func testViewModel() {
        XCTAssertEqual(viewModel.getNumberOfSections(), 1)
        XCTAssertEqual(viewModel.getNumberOfRows(), 2)
        
        // Tests CellModel and ViewModel with all valid fields in the dataModel
        let validCellModel = viewModel.modelFor(0)
        XCTAssertEqual(validCellModel.dbn, "012M1")
        XCTAssertEqual(validCellModel.schoolName, "Clifton High School")
        XCTAssertEqual(validCellModel.address, "250 Vesey Street, NY, 10281, NY")
        XCTAssertEqual(validCellModel.phone, "📞 999-999-9999")
        XCTAssertEqual(validCellModel.latitude, 111.12)
        XCTAssertEqual(validCellModel.longitude, 120.11)
        XCTAssertEqual(validCellModel.overviewParagraph, "Great School")
        XCTAssertEqual(validCellModel.website, "www.google.com")
        XCTAssertEqual(validCellModel.grades, "Grades: 6-12")
        XCTAssertEqual(validCellModel.totalStudents, "200 students")
        
        // Tests CellModel and ViewModel with invalid and missing fields
        
        let invalidCellModel = viewModel.modelFor(1)
        XCTAssertEqual(invalidCellModel.dbn, "012M1")
        XCTAssertEqual(invalidCellModel.schoolName, "Not Available")
        XCTAssertEqual(invalidCellModel.address, "Address Not available")
        XCTAssertEqual(invalidCellModel.phone, "📞 N/A")
        XCTAssertEqual(invalidCellModel.latitude, 0.0)
        XCTAssertEqual(invalidCellModel.longitude, 0.0)
        XCTAssertEqual(invalidCellModel.overviewParagraph, "No Overview Available")
        XCTAssertEqual(invalidCellModel.grades, "Grades: N/A")
        XCTAssertEqual(invalidCellModel.totalStudents, "")
    }
    
    // Tests for FilterHelper
    
    func testFilterHelper() {
        let schools = [NYCSchool(dbn: "012M1", schoolName: "Clifton High School", overviewParagraph: "Great School", primaryAddressLine1: "250 Vesey Street", city: "NY", zip: "10281", stateCode: "NY", website: "www.google.com", phoneNumber: "999-999-9999", latitude: "111.12", longitude: "120.11", grades: "6-12", totalStudents: "200"),
                       NYCSchool(dbn: "012M1", schoolName: nil, overviewParagraph: nil, primaryAddressLine1: nil, city: nil, zip: nil, stateCode: nil, website: nil, phoneNumber: nil, latitude: nil, longitude: nil, grades: nil, totalStudents: nil)]
        
        let filterHelper = NYCSchoolsFilterResultsHelper(schools)
        
        // Test with search text
        filterHelper.filterResults("Clif") { (filteredSchools) in
            XCTAssertEqual(filteredSchools.count, 1)
            XCTAssertEqual(filteredSchools[0].schoolName, "Clifton High School")
        }
        
        // Test with empty search text
        
        filterHelper.filterResults("") { (filteredSchools) in
            XCTAssertEqual(filteredSchools.count, 2)
            XCTAssertEqual(filteredSchools[0].schoolName, "Clifton High School")
        }
    }
    
    // Tests for ViewModel with Filtered results
    
    func testViewModelWithFilteredResults() {
        let schools = [NYCSchool(dbn: "012M1", schoolName: "Clifton High School", overviewParagraph: "Great School", primaryAddressLine1: "250 Vesey Street", city: "NY", zip: "10281", stateCode: "NY", website: "www.google.com", phoneNumber: "999-999-9999", latitude: "111.12", longitude: "120.11", grades: "6-12", totalStudents: "200"),
                       NYCSchool(dbn: "012M1", schoolName: nil, overviewParagraph: nil, primaryAddressLine1: nil, city: nil, zip: nil, stateCode: nil, website: nil, phoneNumber: nil, latitude: nil, longitude: nil, grades: nil, totalStudents: nil)]
        
        let viewModel = NYCSchoolsViewModel(schools, filteredSchools: schools, showAllResults: false)
        XCTAssertEqual(viewModel.getNumberOfSections(), 1)
        XCTAssertEqual(viewModel.getNumberOfRows(), 2)
        
        // Tests CellModel and ViewModel with all valid fields in the dataModel
        let validCellModel = viewModel.modelFor(0)
        XCTAssertEqual(validCellModel.dbn, "012M1")
        XCTAssertEqual(validCellModel.schoolName, "Clifton High School")
        XCTAssertEqual(validCellModel.address, "250 Vesey Street, NY, 10281, NY")
        XCTAssertEqual(validCellModel.phone, "📞 999-999-9999")
        XCTAssertEqual(validCellModel.latitude, 111.12)
        XCTAssertEqual(validCellModel.longitude, 120.11)
        XCTAssertEqual(validCellModel.overviewParagraph, "Great School")
        XCTAssertEqual(validCellModel.website, "www.google.com")
    }
}
