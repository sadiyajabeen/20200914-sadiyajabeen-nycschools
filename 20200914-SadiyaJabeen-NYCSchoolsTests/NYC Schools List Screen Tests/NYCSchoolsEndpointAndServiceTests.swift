//
//  NYCSchoolsServiceTests.swift
//  20200914-SadiyaJabeen-NYCSchoolsTests
//
//  Created by Sadiya Syeda on 9/12/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import XCTest
@testable import _0200914_SadiyaJabeen_NYCSchools

class NYCSchoolsEndpointAndServiceTests: XCTestCase {

    // Valid Mock Url
    private var validUrl: URL? {
        let bundle = Bundle(for: classForCoder)
        guard let url = bundle.url(forResource: "MockNycSchools", withExtension: "json") else {
            return nil
        }
        
        return url
    }
    
    private var inValidUrl: URL? = nil // Invalid Url
    
    override func setUp() {}
    override func tearDown() {}
    
    // Tests for valid NYCSchoolsEndpoint and NYCSchoolsService
    func testNYCSchoolsValidEndpointAndService() {
        let successfulEndpoint = NYCSchoolsEndpoint(validUrl, currentPage: 0)
        let successfulService = NYCSchoolsService(successfulEndpoint)
        
        successfulService.fetchSchools { (result) in
            switch result {
            case let .success(nycSchools):
                XCTAssertEqual(nycSchools.count, 3)
            case let .fail(error):
                XCTAssertEqual(error.description, "Invalid Endpoint")
            }
        }
    }
    
    // Tests for invalid NYCSchoolsEndpoint and NYCSchoolsService
    func testNYCSchoolsInvalidEndpointAndService() {
        let invalidEndpoint = NYCSchoolsEndpoint(inValidUrl, currentPage: 0)
        let invalidService = NYCSchoolsService(invalidEndpoint)
        
        invalidService.fetchSchools { (result) in
            switch result {
            case let .success(nycSchools):
                XCTAssertEqual(nycSchools.count, 3)
            case let .fail(error):
                XCTAssertEqual(error.description, "Invalid Endpoint")
            }
        }
    }
}
