//
//  NYCSchoolDetailsViewModelCellModelTests.swift
//  20200914-SadiyaJabeen-NYCSchoolsTests
//
//  Created by Sadiya Syeda on 9/12/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import XCTest
@testable import _0200914_SadiyaJabeen_NYCSchools

class NYCSchoolDetailsViewModelCellModelTests: XCTestCase {
    var viewModel: NYCDetailsViewModel = {
        let selectedDetailsModel = NYCSelectedSchoolDetailsModel(dbn: "012M1", schoolName: "Clifton High School", numOfSatTestTakers: "200", satCriticalReadingAvgScore: "100", satMathAvgScore: "400", satWritingAvgScore: "500")
        let nycSchoolDetailModel = NYCSchoolDetailModel(dbn: "012M1", schoolName: "Clifton High School", address: "221 Vesey Streen, NY, 10212", overviewParagraph: "Great School", latitude: 100, longitude: 200, phone: "999-999-9999", website: "www.google.com")
        
        return NYCDetailsViewModel(selectedDetailsModel, nycSchoolDetailModel: nycSchoolDetailModel)
    }()
    
    var viewModelInvalidFields: NYCDetailsViewModel = {
        let selectedDetailsModel = NYCSelectedSchoolDetailsModel(dbn: "", schoolName: nil, numOfSatTestTakers: nil, satCriticalReadingAvgScore: nil, satMathAvgScore: nil, satWritingAvgScore: nil)
        let nycSchoolDetailModel = NYCSchoolDetailModel(dbn: "", schoolName: nil, address: nil, overviewParagraph: nil, latitude: nil, longitude: nil, phone: nil, website: "")
        
        return NYCDetailsViewModel(selectedDetailsModel, nycSchoolDetailModel: nycSchoolDetailModel)
    }()
    
    override func setUp() {}
    
    override func tearDown() {}
    
    // Tests for ViewModel and Cell Model with Valid
    func testViewModel() {
        XCTAssertEqual(viewModel.getNumberOfSections(), 4)
        XCTAssertEqual(viewModelInvalidFields.getNumberOfRows(.satResults), 1)
        XCTAssertEqual(viewModelInvalidFields.getNumberOfRows(.contact), 1)
        XCTAssertEqual(viewModelInvalidFields.getNumberOfRows(.overview), 1)
        XCTAssertEqual(viewModelInvalidFields.getNumberOfRows(.schoolMap), 1)
        
        let validScreenModel = viewModel.getScreenModel()
        XCTAssertEqual(validScreenModel.schoolName, "Clifton High School")
        XCTAssertEqual(validScreenModel.address, "Address: 221 Vesey Streen, NY, 10212")
        XCTAssertEqual(validScreenModel.satCriticalReadingAvgScore, "Critical Reading Avg Score = 100")
        XCTAssertEqual(validScreenModel.satMathAvgScore, "SAT Math Avg Score = 400")
        XCTAssertEqual(validScreenModel.satWritingAvgScore, "SAT Writing Average Score = 500")
        XCTAssertEqual(validScreenModel.overviewParagraph, "Great School")
        XCTAssertEqual(validScreenModel.phone, "Phone: 999-999-9999")
        XCTAssertEqual(validScreenModel.website, "www.google.com")
        XCTAssertEqual(validScreenModel.latitude, 100)
        XCTAssertEqual(validScreenModel.longitude, 200)
    }
    
    // Tests for ViewModel and Cell Model with Missing and Invalid fields
    func testMissingAndInvalidFieldsViewModel() {
        XCTAssertEqual(viewModel.getNumberOfSections(), 4)
        XCTAssertEqual(viewModelInvalidFields.getNumberOfRows(.satResults), 1)
        XCTAssertEqual(viewModelInvalidFields.getNumberOfRows(.contact), 1)
        XCTAssertEqual(viewModelInvalidFields.getNumberOfRows(.overview), 1)
        XCTAssertEqual(viewModelInvalidFields.getNumberOfRows(.schoolMap), 1)
        
        let invalidScreenModel = viewModelInvalidFields.getScreenModel()
        XCTAssertEqual(invalidScreenModel.schoolName, "N/A")
        XCTAssertEqual(invalidScreenModel.address, "Address: N/A")
        XCTAssertEqual(invalidScreenModel.satCriticalReadingAvgScore, "Critical Reading Avg Score = N/A")
        XCTAssertEqual(invalidScreenModel.satMathAvgScore, "SAT Math Avg Score = N/A")
        XCTAssertEqual(invalidScreenModel.satWritingAvgScore, "SAT Writing Average Score = N/A")
        XCTAssertEqual(invalidScreenModel.overviewParagraph, "This school doesn't have overview")
        XCTAssertEqual(invalidScreenModel.phone, "Phone: N/A")
        XCTAssertEqual(invalidScreenModel.website, "")
        XCTAssertEqual(invalidScreenModel.latitude, 0.0)
        XCTAssertEqual(invalidScreenModel.longitude, 0.0)
    }
}
