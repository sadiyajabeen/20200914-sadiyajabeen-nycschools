//
//  NYCSchoolDetailsEndpointAndServiceTests.swift
//  20200914-SadiyaJabeen-NYCSchoolsTests
//
//  Created by Sadiya Syeda on 9/12/20.
//  Copyright © 2020 Sadiya Syeda. All rights reserved.
//

import XCTest
@testable import _0200914_SadiyaJabeen_NYCSchools

class NYCSchoolDetailsEndpointAndServiceTests: XCTestCase {
    // Valid Mock Url
    private var validUrl: URL? {
        let bundle = Bundle(for: classForCoder)
        guard let url = bundle.url(forResource: "MockNycSchoolDetailValidFields", withExtension: "json") else {
            return nil
        }
        
        return url
    }
    
    private var inValidUrl: URL? = nil // Invalid Url
    
    // Mock Url with missing and invalid fields
    private var missingFieldsUrl: URL? {
        let bundle = Bundle(for: classForCoder)
        guard let url = bundle.url(forResource: "MockNycSchooDetailsInvalidFields", withExtension: "json") else {
            return nil
        }
        
        return url
    }
    
    override func setUp() {}
    override func tearDown() {}
    
    // Tests for valid NYCSchoolDetailsEndpoint and NYCSchoolDetailsService
    func testNYCSchoolDetailsValidEndpointAndService() {
        let queryItem = URLQueryItem(name: "dbn", value: "01M292")
        let successfulEndpoint = NYCSchoolDetailsEndpoint(url: validUrl, queryItems: [queryItem])
        
        let successfulService = NYCSchooDetailService(successfulEndpoint)
        
        successfulService.fetchSchoolDetail { (result) in
            switch result {
            case let .success(schoolDetail):
                XCTAssertEqual(schoolDetail.dbn, "01M292")
                XCTAssertEqual(schoolDetail.numOfSatTestTakers, "29")
                XCTAssertEqual(schoolDetail.satCriticalReadingAvgScore, "355")
                XCTAssertEqual(schoolDetail.satMathAvgScore, "404")
                XCTAssertEqual(schoolDetail.satWritingAvgScore, "363")
                XCTAssertEqual(schoolDetail.schoolName, "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES")
            case let .fail(error):
                print(error.description)
            }
        }
    }
    
    // Tests for invalid NYCSchoolsEndpoint and NYCSchoolsService
    func testNYCSchoolsInvalidEndpointAndService() {
        let queryItem = URLQueryItem(name: "dbn", value: "01M292")
        let invalidEndpoint = NYCSchoolDetailsEndpoint(url: inValidUrl, queryItems: [queryItem])
        let invalidService = NYCSchooDetailService(invalidEndpoint)
        
        invalidService.fetchSchoolDetail { result in
            switch result {
            case let .success(schoolDetail):
                print("Successful response \(schoolDetail)")
            case let .fail(error):
                XCTAssertEqual(error.description, "Invalid Endpoint")
            }
        }
    }
    
    // Tests for invalid and missing fields NYCSchoolDetailsEndpoint and NYCSchoolDetailsService
    func testNYCSchoolDetailsInvalidMissingValidEndpointAndService() {
        let queryItem = URLQueryItem(name: "dbn", value: "01M292")
        let successfulEndpoint = NYCSchoolDetailsEndpoint(url: missingFieldsUrl, queryItems: [queryItem])
        
        let successfulService = NYCSchooDetailService(successfulEndpoint)
        
        successfulService.fetchSchoolDetail { (result) in
            switch result {
            case let .success(schoolDetail):
                print("Successful response \(schoolDetail)")
            case let .fail(error):
                XCTAssertEqual(error.description, "Failed to parse data object to array of NYCSchools")
            }
        }
    }

}
